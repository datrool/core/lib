# -*- coding: utf-8 -*-
from logging import getLogger
from src.logger import set_logger


def test_set_logger():
	logger = set_logger("logger-name", screen=True, path="/tmp")
	assert logger is not None

	instance_2 = getLogger("logger-name")
	assert logger == instance_2
