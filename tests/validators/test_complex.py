# -*- coding: utf-8 -*-
from typing import Optional
from src.validators import DictValidator
inner = {
	"one": (str, None),
	"two": (str, None),
	"three": ([str, Optional], None),
	"four": ([[str, int], Optional], None)
}
validator = {
	"complex": (dict, inner),
	"complex_opt": ([dict, Optional], inner),
	"complex_list": (list, inner),
	"complex_list_opt": ([list, Optional], inner),
}


def test_complex_valid():
	base = {
		"one": "A",
		"two": "C",
		"three": "B",
	}
	structure = {
		"complex": base,
		"complex_list": [base, base]
	}
	assert DictValidator.check(structure, validator) is True
	base = {
		"one": "A",
		"two": "C",
		"four": 1
	}
	structure = {
		"complex": base,
		"complex_opt": base,
		"complex_list": [base, base],
		"complex_list_opt": [base, base]
	}
	assert DictValidator.check(structure, validator) is True
	structure = {
		"complex": base,
		"complex_opt": None,
		"complex_list": [base, base],
		"complex_list_opt": [None, base]
	}
	assert DictValidator.check(structure, validator) is True
	structure = {
		"complex": base,
		"complex_opt": base,
		"complex_list": [base, base],
		"complex_list_opt": None
	}
	assert DictValidator.check(structure, validator) is True


def test_complex_invalid():
	base = {
		"one": "A",
		"two": "C",
		"three": "B",
	}
	structure = {
		"complex": {"one": "A", "twod": "D"},
		"complex_opt": base,
		"complex_list": [base, base],
		"complex_list_opt": [base, base]
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"complex": None,
		"complex_opt": base,
		"complex_list": [base, base],
		"complex_list_opt": [base, base]
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"complex": "base",
		"complex_list": [base, base],
		"complex_list_opt": [base, base]
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"complex": base,
		"complex_opt": base,
		"complex_list": [{}, base],
		"complex_list_opt": [base, base]
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"complex": base,
		"complex_opt": base,
		"complex_list": ["base", base],
		"complex_list_opt": [base, base]
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"complex": base,
		"complex_opt": base,
		"complex_list": [base, base],
		"complex_list_opt": [base, 1]
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"complex": base,
		"complex_opt": base,
		"complex_list": [base, base],
		"complex_list_opt": [base, {"one": "A"}]
	}
	assert DictValidator.check(structure, validator) is False
	base = {
		"one": "A",
		"two": "C",
		"four": 1.1
	}
	structure = {
		"complex": base,
		"complex_opt": base,
		"complex_list": [base, base],
		"complex_list_opt": [base, base]
	}
	assert DictValidator.check(structure, validator) is False
