# -*- coding: utf-8 -*-
from typing import Optional
from src.validators import DictValidator


class VC:
	def __init__(self):
		pass


class NVC:
	def __init__(self):
		pass


validator = {
	"one": (str, None),
	"two": (int, None),
	"three": (float, None),
	"four": (VC, None),
	"five": (bool, None),
	"six": ((list, Optional), int)
}
validator_list = {
	"list": ((int, str), None)
}


def test_valid():
	structure = {
		"one": "A",
		"two": 1,
		"three": 0.1,
		"four": VC(),
		"five": False,
		"six": [1, 2, 3]
	}
	assert DictValidator.check(structure, validator) is True


def test_invalid_string():
	structure = {
		"one": None,
		"two": 1,
		"three": 0.1,
		"four": VC(),
		"five": False,
	}
	assert DictValidator.check(structure, validator) is False
	structure["one"] = 1
	assert DictValidator.check(structure, validator) is False
	structure["one"] = 0.1
	assert DictValidator.check(structure, validator) is False
	structure["one"] = False
	assert DictValidator.check(structure, validator) is False
	structure["one"] = VC()
	assert DictValidator.check(structure, validator) is False


def test_invalid_int():
	structure = {
		"one": "A",
		"two": None,
		"three": 0.1,
		"four": VC(),
		"five": False,
	}
	assert DictValidator.check(structure, validator) is False
	structure["two"] = "1"
	assert DictValidator.check(structure, validator) is False
	structure["two"] = 0.1
	assert DictValidator.check(structure, validator) is False
	structure["two"] = VC()
	assert DictValidator.check(structure, validator) is False


def test_invalid_float():
	structure = {
		"one": "A",
		"two": 1,
		"three": None,
		"four": VC(),
		"five": False,
	}
	assert DictValidator.check(structure, validator) is False
	structure["three"] = 1
	assert DictValidator.check(structure, validator) is False
	structure["three"] = "0.1"
	assert DictValidator.check(structure, validator) is False
	structure["three"] = False
	assert DictValidator.check(structure, validator) is False
	structure["three"] = VC()
	assert DictValidator.check(structure, validator) is False


def test_invalid_class():
	structure = {
		"one": "A",
		"two": 1,
		"three": 0.1,
		"four": None,
		"five": False,
	}
	assert DictValidator.check(structure, validator) is False
	structure["four"] = NVC()
	assert DictValidator.check(structure, validator) is False
	structure["four"] = 1
	assert DictValidator.check(structure, validator) is False
	structure["four"] = "0.1"
	assert DictValidator.check(structure, validator) is False
	structure["four"] = False
	assert DictValidator.check(structure, validator) is False
	structure["four"] = 0.1
	assert DictValidator.check(structure, validator) is False


def test_invalid_bool():
	structure = {
		"one": "A",
		"two": 1,
		"three": 0.1,
		"four": VC(),
		"five": None,
	}
	assert DictValidator.check(structure, validator) is False
	structure["five"] = 1
	assert DictValidator.check(structure, validator) is False
	structure["five"] = "0.1"
	assert DictValidator.check(structure, validator) is False
	structure["five"] = 0.1
	assert DictValidator.check(structure, validator) is False
	structure["five"] = VC()
	assert DictValidator.check(structure, validator) is False


def test_invalid_list():
	structure = {
		"one": "A",
		"two": 1,
		"three": 0.1,
		"four": VC(),
		"five": True,
		"six": ["A"]
	}
	assert DictValidator.check(structure, validator) is False


def test_list_valid():
	structure = {
		"list": 3
	}
	assert DictValidator.check(structure, validator_list) is True


def test_list_invalid():
	structure = {
		"list": 3.2
	}
	assert DictValidator.check(structure, validator_list) is False
