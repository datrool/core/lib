# -*- coding: utf-8 -*-
from typing import Optional
from src.validators import DictValidator
validator = {
	"one": (str, None),
	"two": (str, None),
	"three": ([str, Optional], None)
}


def test_optional_valid_present():
	structure = {
		"one": "A",
		"two": "C",
		"three": "B",
	}
	assert DictValidator.check(structure, validator) is True


def test_optional_valid_not_present():
	structure = {
		"one": "A",
		"two": "C",
	}
	assert DictValidator.check(structure, validator) is True


def test_optional_valid_none():
	structure = {
		"one": "A",
		"two": "C",
		"three": None,
	}
	assert DictValidator.check(structure, validator) is True


def test_optional_invalid_present():
	structure = {
		"two": "C",
		"three": "B",
	}
	assert DictValidator.check(structure, validator) is False
	assert DictValidator.check({"two": "C"}, validator) is False
	assert DictValidator.check({}, validator) is False
