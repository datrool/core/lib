# -*- coding: utf-8 -*-
from typing import Optional
from src.validators import DictValidator
validator = {
	"one": (str, None),
	"two": (str, None),
}


def test_exact_valid():
	structure = {
		"one": "A",
		"two": "C",
	}
	assert DictValidator.check(structure, validator, exact_keys=True) is True


def test_exact_invalid():
	structure = {
		"one": "A",
		"two": "C",
		"three": "B"
	}
	assert DictValidator.check(structure, validator, exact_keys=True) is False

