# -*- coding: utf-8 -*-
from typing import Optional
from src.validators import DictValidator
validator = {
	"one": (str, lambda x, *a: len(x) == 4),
	"two": (int, lambda x, *a: x == 2),
	"three": ([bool, Optional], lambda x, *a: x == True)
}


def test_lambda_valid():
	structure = {
		"one": "AAAA",
		"two": 2,
	}
	assert DictValidator.check(structure, validator) is True
	structure["three"] = True
	assert DictValidator.check(structure, validator) is True


def test_lambda_invalid():
	structure = {
		"one": "AAA",
		"two": 2,
		"three": True
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"one": "AAAA",
		"two": 1,
		"three": True
	}
	assert DictValidator.check(structure, validator) is False
	structure = {
		"one": "AAAA",
		"two": 2,
		"three": False
	}
	assert DictValidator.check(structure, validator) is False
