# -*- coding: utf-8 -*-
from typing import Optional
from src.validators import ListOfDictValidator
validator = {
	"one": (str, None),
	"two": (str, None),
	"three": ([str, Optional], None)
}


def test_list_of_dict_valid():
	structure = {
		"one": "A",
		"two": "C",
		"three": "B",
	}
	list_structure = [structure, structure, structure]
	assert ListOfDictValidator.check(list_structure, validator) is True


def test_list_of_dict_invalid():
	structure = {
		"one": "A",
		"two": "C",
		"three": "B",
	}
	list_structure = [structure, structure, structure, {"one": "B"}]
	assert ListOfDictValidator.check(list_structure, validator) is False
