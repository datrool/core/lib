# -*- coding: utf-8 -*-
import time
from datetime import datetime, timedelta
from uuid import UUID
from freezegun import freeze_time
from src.scheduler import Scheduler


def callback():
	assert True


def test_invalid_parameter():
	exception = False
	try:
		s = Scheduler()
		s.every(callback, year=2)
	except ValueError:
		exception = True
	assert exception is True


def test_time_passed():
	exception = False
	try:
		target_time = datetime.now() - timedelta(seconds=1)
		s = Scheduler()
		s.at(target_time, callback)
	except ValueError:
		exception = True
	assert exception is True


@freeze_time("2022-01-01 01:00:00")
def test_invalid_day_weekday():
	exception = False
	try:
		s = Scheduler()
		s.every(callback, day=2, weekday="monday")
	except ValueError:
		exception = True
	assert exception is True


def test_delay():

	delay = 1
	s = Scheduler()
	uuid = s.delay(delay, callback)
	assert isinstance(uuid, UUID)
	time.sleep(delay + 0.1)  # Wait for the scheduler to execute the callback
	assert uuid not in s._Scheduler__queue  # Ensure the callback has been removed from the queue


def test_clear():

	delay = 1
	s = Scheduler()
	s.delay(delay, callback)
	assert len(s._Scheduler__queue) == 1
	s.clear()
	assert len(s._Scheduler__queue) == 0


def test_at():
	callback_called = {
		"return_value": False
	}

	def callback():
		callback_called["return_value"] = True

	target_time = datetime.now() + timedelta(seconds=1)
	s = Scheduler()
	uuid = s.at(target_time, callback)
	assert isinstance(uuid, UUID)
	assert callback_called["return_value"] is False
	time.sleep(1.1)  # Wait for the scheduler to execute the callback
	assert uuid not in s._Scheduler__queue  # Ensure the callback has been removed from the queue
	assert callback_called["return_value"] is True


def test_cancel():
	delay = 1
	s = Scheduler()
	uuid = s.delay(delay, callback)
	assert len(s._Scheduler__queue) == 1
	s.cancel(uuid)
	assert len(s._Scheduler__queue) == 0
	exception = False
	try:
		s.cancel(uuid)
	except ReferenceError:
		exception = True
	assert exception is True


# @freeze_time("2022-01-01 01:00:00")
def test_every():
	callback_called = {
		"return_value": 0
	}

	def callback():
		callback_called["return_value"] += 1

	target_kwargs = {"second": int((time.time() + 2) % 60)}
	s = Scheduler()
	uuid = s.every(callback, **target_kwargs)

	assert callback_called["return_value"] == 0
	time.sleep(2.1)
	assert callback_called["return_value"] == 1
	s.clear()


def test_next_iterator_weekday():
	weekday_template = {
		"weekday": datetime.now().weekday() - 1,
		"day": None,
		"hour": None,
		"minute": None,
		"second": 0,
	}
	s = Scheduler()
	val = s._Scheduler__next(weekday_template, timedelta(weeks=2))


def test_next_iterator_day():
	day_template = {
		"weekday": None,
		"day": 1,
		"hour": None,
		"minute": None,
		"second": 0,
	}
	s = Scheduler()
	val = s._Scheduler__next(day_template, timedelta(weeks=10))


def test_next_iterator_hour():
	hour_template = {
		"weekday": None,
		"day": None,
		"hour": 1,
		"minute": None,
		"second": 0,
	}
	s = Scheduler()
	val = s._Scheduler__next(hour_template, timedelta(weeks=1))


def test_next_iterator_minute():
	minute_template = {
		"weekday": None,
		"day": None,
		"hour": None,
		"minute": 1,
		"second": 0,
	}
	s = Scheduler()
	val = s._Scheduler__next(minute_template, timedelta(weeks=1))
