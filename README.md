# datrool-lib

Common library for the datrool project.


## Installation

Install package from PYPI repository
```sh
python -m pip install datrool-lib
```

## Contains
- Simple templating validator for complex `dict` and `list` stuctures
- Time based task scheduling class

## License

Created by Patrik Katreňák and released under General Public License v3.0.
